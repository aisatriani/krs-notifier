package aan.krs;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;

import java.util.Calendar;
import java.util.List;

import aan.krs.api.ApiGenerator;
import aan.krs.api.ApiResponse;
import aan.krs.api.HttpClientService;
import aan.krs.db.DataKRS;
import aan.krs.model.Dosen;
import aan.krs.model.Krs;
import aan.krs.model.Mahasiswa;
import aan.krs.services.CheckSetujuiKRSService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private RelativeLayout contentMain;
    private TextView welcomeText;
    private EditText editTahun;
    private EditText editSemester;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        if(!Pref.getInstance(this).isLoginIn()){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            this.finish();
            return;
        }

        if(Pref.getInstance(this).getDataDosen() != null){
            Intent intent = new Intent(this, DosenMainActivity.class);
            startActivity(intent);
            this.finish();
            return;
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setNavHeaderContent(navigationView);

        contentMain = (RelativeLayout) findViewById(R.id.content_main);
        welcomeText = (TextView) findViewById(R.id.welcome_text);
        editTahun = (EditText)findViewById(R.id.text_tahun);
        editTahun.setInputType(InputType.TYPE_NULL);
        editTahun.setOnClickListener(this);
        editSemester = (EditText) findViewById(R.id.text_semester);
        editSemester.setInputType(InputType.TYPE_NULL);
        editSemester.setOnClickListener(this);
        findViewById(R.id.button_tampil).setOnClickListener(this);

        welcomeText.setText("Selamat datang "+Pref.getInstance(context).getDataMahasiswa().getNAMA());

        subscribeFireBaseNotifikasi();

        if(!Pref.getInstance(this).isFirstLoad()){
            loadKrsForNotification();
        }



    }

    private void loadKrsForNotification() {
        HttpClientService clientService = ApiGenerator.createService(HttpClientService.class);
        Call<ApiResponse<List<Krs.MataKuliah>>> call = clientService.cariKRS(2016, 2, Pref.getInstance(this).getNim());
        call.enqueue(new Callback<ApiResponse<List<Krs.MataKuliah>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<Krs.MataKuliah>>> call, Response<ApiResponse<List<Krs.MataKuliah>>> response) {
                if(response.isSuccessful()){
                    List<Krs.MataKuliah> mataKuliahs = response.body().getData();
                    //List<DataKRS> dataKRSes = DataKRS.listAll(DataKRS.class);
                    //if(dataKRSes.size() == 0){
                        for (Krs.MataKuliah mk : mataKuliahs){
                            DataKRS dataKRS = new DataKRS();
                            dataKRS.setStatus(mk.getSTATUS());
                            dataKRS.setNamamakul(mk.getNAMAMAKUL());
                            dataKRS.setKodemakul(mk.getKODEMAKUL());
                            dataKRS.setKeterangan(mk.getKETERANGAN());
                            dataKRS.save();
                        }
                    //}

                    CheckSetujuiKRSService.startAction(MainActivity.this,2016,2);

                    Pref.getInstance(MainActivity.this).setFirstLoad(true);
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<Krs.MataKuliah>>> call, Throwable t) {
                t.printStackTrace();
                loadKrsForNotification();
            }
        });
    }

    private void setNavHeaderContent(NavigationView navigationView) {
        View header=navigationView.getHeaderView(0);
        TextView nav_title = (TextView) header.findViewById(R.id.title_nav_header);
        TextView nav_desc = (TextView) header.findViewById(R.id.desc_nav_header);
        ImageView imageView = (ImageView) header.findViewById(R.id.imageView);
        if(Pref.getInstance(this).getDataDosen() != null){
            Dosen dosen = Pref.getInstance(this).getDataDosen();
            nav_title.setText(dosen.getNAMA());
            nav_desc.setText(dosen.getNIDN());
            imageView.setImageResource(R.drawable.dosen);
        }

        if(Pref.getInstance(this).getDataMahasiswa() != null){
            Mahasiswa mahasiswa = Pref.getInstance(this).getDataMahasiswa();
            nav_title.setText(mahasiswa.getNAMA());
            nav_desc.setText(Pref.getInstance(this).getNim());
            imageView.setImageResource(R.drawable.user);
        }


    }

    private void subscribeFireBaseNotifikasi() {
        if(Pref.getInstance(this).getDataDosen() != null)
            FirebaseMessaging.getInstance().subscribeToTopic(Pref.getInstance(this).getNidn());
        if(Pref.getInstance(this).getDataMahasiswa() != null)
            FirebaseMessaging.getInstance().subscribeToTopic(Pref.getInstance(this).getNim());

        Log.e("firebase subscribe","register topic " + Pref.getInstance(this).getNidn());
    }

    private void unsubscribeNotification(){
        if(Pref.getInstance(this).getDataDosen() != null)
            FirebaseMessaging.getInstance().unsubscribeFromTopic(Pref.getInstance(this).getNidn());

        if(Pref.getInstance(this).getDataMahasiswa() != null)
            FirebaseMessaging.getInstance().unsubscribeFromTopic(Pref.getInstance(this).getNim());

        Log.e("firebase unsubscribe","unregister topic " + Pref.getInstance(this).getNidn());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout) {
            // Handle the camera action
            doActionLogOut();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void doActionLogOut() {
        unsubscribeNotification();
        Pref.getInstance(context).clearAllData();
        finish();
        startActivity(getIntent());
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_tampil:
                if(editTahun.getText().toString().equals("2017")){
                    doActionTampilKontrakKrs();
                    return;
                }

                doActionTampilKrs();

                break;
            case R.id.text_tahun:
//                loadDialogTahun();
                loadDialogTahunAlert();
                break;

            case R.id.text_semester:
                loadDialogSemester();
                break;
        }
    }

    private void doActionTampilKrs() {
        Intent intent = new Intent(this, KrsActivity.class);
        intent.putExtra("tahun",editTahun.getText().toString());
        intent.putExtra("semester",editSemester.getText().toString());
        startActivity(intent);
    }

    private void doActionTampilKontrakKrs() {
        Intent intent = new Intent(this, KontrakKrsActivity.class);
        intent.putExtra("tahun",editTahun.getText().toString());
        intent.putExtra("semester",editSemester.getText().toString());
        startActivity(intent);
    }

    private void loadDialogSemester() {
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle("Semster");
        String[] types = {"Ganjil", "Genap"};
        b.setItems(types, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                switch(which){
                    case 0:
                        //onZipRequested();
                        editSemester.setText("Ganjil");
                        break;
                    case 1:
                        //onCategoryRequested();
                        editSemester.setText("Genap");
                        break;
                }
            }

        });

        b.show();
    }

    private void loadDialogTahun() {
        DialogFragment dialogYear = new YearPicker();
        dialogYear.show(getSupportFragmentManager(), "datePicker");

    }

    private void loadDialogTahunAlert()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Pilih tahun akademik");

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);

        final NumberPicker numberPicker = new NumberPicker(this);
        numberPicker.setMinValue(2000);
        numberPicker.setMaxValue(3000);
        numberPicker.setValue(year);

        builder.setView(numberPicker);
        builder.setPositiveButton("Pilih", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                editTahun.setText(String.valueOf(numberPicker.getValue()));
            }
        });
        builder.create().show();
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
        }
    }

    public static class YearPicker extends DialogFragment implements NumberPicker.OnValueChangeListener {

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            NumberPicker numberPicker = new NumberPicker(getActivity());
            numberPicker.setMinValue(2000);
            numberPicker.setMaxValue(3000);
            numberPicker.setValue(year);
            Dialog dialog = super.onCreateDialog(savedInstanceState);
            dialog.setContentView(numberPicker);
            return dialog;
        }

        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            Toast.makeText(getActivity(), "test", Toast.LENGTH_SHORT).show();
        }
    }
}
