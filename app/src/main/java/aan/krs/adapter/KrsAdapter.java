package aan.krs.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import aan.krs.R;
import aan.krs.model.KontrakKrs;
import aan.krs.model.Krs;

public class KrsAdapter extends RecyclerView.Adapter<KrsAdapter.ViewHolder> {

    private List<Krs.MataKuliah> list;
    private ClickListener clickListener;
    private Context context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        TextView tv_title, tv_desc, tv_setujui;
        ImageView imgIccn;
        View v;


        ViewHolder(View v) {

            super(v);
            this.v = v;

            tv_title = (TextView)v.findViewById(R.id.text_title);
            tv_desc = (TextView)v.findViewById(R.id.text_desc);
            tv_setujui = (TextView)v.findViewById(R.id.text_setujui);
            imgIccn = (ImageView)v.findViewById(R.id.img_icon);


        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public KrsAdapter(Context context, List<Krs.MataKuliah> myDataset) {
        this.context = context;
        list = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_kontrak_krs, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        //holder.tv_title.setText(list.get(position).getNama_tour());

        holder.tv_title.setText(list.get(position).getNAMAMAKUL());
        holder.tv_desc.setText(list.get(position).getKODEMAKUL());
        if(list.get(position).getKETERANGAN().equals("DISETUJUI")){
            holder.tv_setujui.setText("Disetujui");
            holder.imgIccn.setImageResource(R.drawable.yes);
        }

        if(list.get(position).getKETERANGAN().equals("TIDAK DISETUJUI")){
            holder.tv_setujui.setText("Tidak Disetujui");
            holder.imgIccn.setImageResource(R.drawable.non);
        }

        if(list.get(position).getKETERANGAN().equals("BELUM DISETUJUI")){
            holder.tv_setujui.setText("Belum Disetujui");
            holder.imgIccn.setImageResource(R.drawable.belum);
        }

        holder.v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null)
                    clickListener.onClick(view, position);
            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ClickListener {
        void onClick(View v, int position);
    }

    public void setClickListener(ClickListener listener) {
        this.clickListener = listener;
    }

}