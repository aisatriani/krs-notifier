package aan.krs;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import aan.krs.api.ApiGenerator;
import aan.krs.api.ApiResponse;
import aan.krs.api.HttpClientService;
import aan.krs.model.Dosen;
import aan.krs.model.Mahasiswa;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout activityLogin;
    private Spinner spinnerTipeLogin;
    private EditText editUsername;
    private EditText editPassword;
    private ProgressDialog pd;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.context = this;

        activityLogin = (RelativeLayout) findViewById(R.id.activity_login);
        spinnerTipeLogin = (Spinner) findViewById(R.id.spinner_tipe_login);
        editUsername = (EditText)findViewById(R.id.edit_username);
        editPassword = (EditText) findViewById(R.id.edit_password);
        findViewById(R.id.btn_login).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.btn_login){
            if(validate()){
                doSubmitLogin();
            }
        }

    }

    private void doSubmitLogin() {
        pd = new ProgressDialog(this);
        pd.setMessage("Loading..");
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        if(isLoginMahasiswa()){
            doLoginMahasiswa();
        }

        if(isLoginDOsen()){
            doLoginDOsen();
        }



    }

    private void doLoginMahasiswa() {
        HttpClientService clientService = ApiGenerator.createService(HttpClientService.class);
        Call<ApiResponse<Mahasiswa>> call = clientService.loginMahasiswa(editUsername.getText().toString(), editPassword.getText().toString());
        call.enqueue(new Callback<ApiResponse<Mahasiswa>>() {
            @Override
            public void onResponse(Call<ApiResponse<Mahasiswa>> call, Response<ApiResponse<Mahasiswa>> response) {
                pd.dismiss();
                if(response.isSuccessful()){

                    if(response.body().getErr_no() > 0){
                        editUsername.setError("Username atau password salah");
                        editUsername.requestFocus();
                    }

                    //setup login
                    Pref.getInstance(context).setLoginIn(true);
                    Pref.getInstance(context).storeDataMahasiswa(response.body().getData());
                    Pref.getInstance(context).storeNim(editUsername.getText().toString());
                    Intent intent = new Intent(context, MainActivity.class);
                    startActivity(intent);
                    finish();

                }else{
                    if(response.code() == 500){
                        Snackbar.make(findViewById(R.id.activity_login), getString(R.string.response_error), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getString(R.string.ulangi), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        doLoginMahasiswa();
                                    }
                                })
                                .show();
                    }
                    if(response.code() == 401){
                        editUsername.setError("Username atau password salah");
                        editUsername.requestFocus();
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Mahasiswa>> call, Throwable t) {
                pd.dismiss();
                t.printStackTrace();
                Snackbar.make(findViewById(R.id.activity_login), getString(R.string.gagal_connect), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.ulangi), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                doLoginMahasiswa();
                            }
                        })
                        .show();
            }
        });

    }

    private void doLoginDOsen() {
        //Toast.makeText(this, "login dosen", Toast.LENGTH_SHORT).show();
        HttpClientService clientService = ApiGenerator.createService(HttpClientService.class);
        Call<ApiResponse<Dosen>> call = clientService.loginDosen(editUsername.getText().toString(), editPassword.getText().toString());
        call.enqueue(new Callback<ApiResponse<Dosen>>() {
            @Override
            public void onResponse(Call<ApiResponse<Dosen>> call, Response<ApiResponse<Dosen>> response) {
                pd.dismiss();
                if(response.isSuccessful()){

                    if(response.body().getErr_no() > 0){
                        editUsername.setError("Username atau password salah");
                        editUsername.requestFocus();
                        return;
                    }
                    //setup login
                    Pref.getInstance(context).setLoginIn(true);
                    Pref.getInstance(context).storeDataDosen(response.body().getData());
                    Pref.getInstance(context).storeNidn(editUsername.getText().toString());
                    Intent intent = new Intent(context, DosenMainActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    if(response.code() == 500){
                        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                        builder.setMessage("Terjadi Kesalahan saat memuat data, Harap coba lagi nanti");
                        builder.setPositiveButton("Ok",null);
                        builder.create().show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Dosen>> call, Throwable t) {
                t.printStackTrace();
                pd.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setMessage("Gagal terhubung ke server. coba lagi");
                builder.setPositiveButton("Ok",null);
                builder.create().show();
            }
        });
    }



    private boolean validate() {
        EditText[] editTexts = {editUsername, editPassword};
        for (EditText editText : editTexts) {
            if (editText.getText().toString().trim().equalsIgnoreCase("")) {
                editText.setError("tidak boleh kosong");
                editText.requestFocus();
                return false;
            }
        }

        return true;
    }

    private boolean isLoginMahasiswa() {
        String type = spinnerTipeLogin.getSelectedItem().toString();

        if(type.equals("Mahasiswa"))
            return true;

        return false;
    }

    private boolean isLoginDOsen() {
        String type = spinnerTipeLogin.getSelectedItem().toString();

        if(type.equals("Dosen"))
            return true;

        return false;
    }

}
