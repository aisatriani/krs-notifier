package aan.krs;

import android.app.ProgressDialog;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import aan.krs.adapter.MataKuliahKontrakAdapter;
import aan.krs.api.ApiGenerator;
import aan.krs.api.HttpClientService;
import aan.krs.model.KontrakKrs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsKontrakActivity extends AppCompatActivity {

    private RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_kontrak);

        rv = (RecyclerView)findViewById(R.id.rv_matakulih_kontrak);
        rv.setLayoutManager(new LinearLayoutManager(this));

        loadMatakuliahKontrak();
    }

    private void loadMatakuliahKontrak() {

        KontrakKrs kontrakKrs = (KontrakKrs) getIntent().getSerializableExtra("mk_kontrak");

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Loading..");
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        HttpClientService clientService = ApiGenerator.createService(HttpClientService.class);
        Call<List<KontrakKrs>> call = clientService.getMatakulihKontrak(kontrakKrs.getNim(), kontrakKrs.getTahun(), kontrakKrs.getSemester());
        call.enqueue(new Callback<List<KontrakKrs>>() {
            @Override
            public void onResponse(Call<List<KontrakKrs>> call, Response<List<KontrakKrs>> response) {
                pd.dismiss();
                if(response.isSuccessful()){
                    MataKuliahKontrakAdapter adapter = new MataKuliahKontrakAdapter(DetailsKontrakActivity.this, response.body());
                    rv.setAdapter(adapter);
                }
                else{
                    Snackbar.make(findViewById(R.id.activity_details_kontrak), "Gagal menerima respon dari server", Snackbar.LENGTH_INDEFINITE)
                            .setAction("Ulangi", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    loadMatakuliahKontrak();
                                }
                            })
                            .show();
                }

            }

            @Override
            public void onFailure(Call<List<KontrakKrs>> call, Throwable t) {
                pd.dismiss();
                t.printStackTrace();

                Snackbar.make(findViewById(R.id.activity_details_kontrak), "Gagal terhubung ke server", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Ulangi", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loadMatakuliahKontrak();
                            }
                        })
                        .show();
            }
        });

    }
}
