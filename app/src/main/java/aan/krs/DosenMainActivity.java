package aan.krs;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;

import java.util.Calendar;

import aan.krs.model.Dosen;
import aan.krs.model.Mahasiswa;

public class DosenMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private RelativeLayout contentMain;
    private TextView welcomeText;
    private EditText editTahun;
    private EditText editSemester;
    private Context context;
    private LinearLayout menuMahasiswaBimbingan;
    private LinearLayout menuMahasiswaKontrak;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dosen);

        context = this;

        if(!Pref.getInstance(this).isLoginIn()){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            return;
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setNavHeaderContent(navigationView);

        contentMain = (RelativeLayout) findViewById(R.id.content_main);
        welcomeText = (TextView) findViewById(R.id.welcome_text);

        menuMahasiswaBimbingan = (LinearLayout) findViewById(R.id.menu_mahasiswa_bimbingan);
        menuMahasiswaKontrak = (LinearLayout) findViewById(R.id.menu_mahasiswa_kontrak);

        menuMahasiswaBimbingan.setOnClickListener(this);
        menuMahasiswaKontrak.setOnClickListener(this);

        welcomeText.setText("Selamat datang "+Pref.getInstance(context).getDataDosen().getNAMA());

        subscribeFireBaseNotifikasi();

    }

    private void setNavHeaderContent(NavigationView navigationView) {
        View header=navigationView.getHeaderView(0);
        TextView nav_title = (TextView) header.findViewById(R.id.title_nav_header);
        TextView nav_desc = (TextView) header.findViewById(R.id.desc_nav_header);
        ImageView imageView = (ImageView) header.findViewById(R.id.imageView);
        if(Pref.getInstance(this).getDataDosen() != null){
            Dosen dosen = Pref.getInstance(this).getDataDosen();
            nav_title.setText(dosen.getNAMA());
            nav_desc.setText(dosen.getNIDN());
            imageView.setImageResource(R.drawable.dosen);
        }

        if(Pref.getInstance(this).getDataMahasiswa() != null){
            Mahasiswa mahasiswa = Pref.getInstance(this).getDataMahasiswa();
            nav_title.setText(mahasiswa.getNAMA());
            nav_desc.setText(Pref.getInstance(this).getNim());
            imageView.setImageResource(R.drawable.user);
        }


    }

    private void subscribeFireBaseNotifikasi() {

        if(Pref.getInstance(this).getDataDosen() != null)
            FirebaseMessaging.getInstance().subscribeToTopic(Pref.getInstance(this).getNidn());

        if(Pref.getInstance(this).getDataMahasiswa() != null)
            FirebaseMessaging.getInstance().subscribeToTopic(Pref.getInstance(this).getNim());

        Log.e("firebase subscribe","register topic " + Pref.getInstance(this).getNidn());

    }

    private void unsubscribeNotification(){
        if(Pref.getInstance(this).getDataDosen() != null)
            FirebaseMessaging.getInstance().unsubscribeFromTopic(Pref.getInstance(this).getNidn());

        if(Pref.getInstance(this).getDataMahasiswa() != null)
            FirebaseMessaging.getInstance().unsubscribeFromTopic(Pref.getInstance(this).getNim());

        Log.e("firebase unsubscribe","unregister topic " + Pref.getInstance(this).getNidn());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout) {
            unsubscribeNotification();
            Pref.getInstance(context).clearAllData();
            finish();
            startActivity(getIntent());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.menu_mahasiswa_bimbingan:
                Intent intent = new Intent(this, DaftarMahasiswaBimbinganActivity.class);
                startActivity(intent);
                break;

            case R.id.menu_mahasiswa_kontrak:
                Intent intentx = new Intent(this, DaftarMahasiswaKontrakActivity.class);
                startActivity(intentx);
                break;
        }
    }


}
