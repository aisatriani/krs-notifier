package aan.krs;

import android.app.ProgressDialog;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.List;

import aan.krs.adapter.MahasiswaBimbinganAdapter;
import aan.krs.api.ApiGenerator;
import aan.krs.api.HttpClientService;
import aan.krs.model.MahasiswaDb;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DaftarMahasiswaBimbinganActivity extends AppCompatActivity {

    private RelativeLayout activityDaftarMahasiswaBimbingan;
    private RecyclerView rvMhsBimbingan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_mahasiswa_bimbingan);
        setTitle("Mahasiswa Bimbingan");

        activityDaftarMahasiswaBimbingan = (RelativeLayout) findViewById(R.id.activity_daftar_mahasiswa_bimbingan);
        rvMhsBimbingan = (RecyclerView) findViewById(R.id.rv_mhs_bimbingan);
        rvMhsBimbingan.setLayoutManager(new LinearLayoutManager(this));

        loadDataMahasiswa();
    }

    private void loadDataMahasiswa() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Loading..");
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        HttpClientService clientService = ApiGenerator.createService(HttpClientService.class);
        Call<List<MahasiswaDb>> call = clientService.getMahasiswaBimbingan(Pref.getInstance(this).getNidn());
        call.enqueue(new Callback<List<MahasiswaDb>>() {
            @Override
            public void onResponse(Call<List<MahasiswaDb>> call, Response<List<MahasiswaDb>> response) {
                pd.dismiss();
                if(response.isSuccessful()){
                    MahasiswaBimbinganAdapter adapter = new MahasiswaBimbinganAdapter(DaftarMahasiswaBimbinganActivity.this, response.body());
                    rvMhsBimbingan.setAdapter(adapter);
                }else{
                    Snackbar.make(findViewById(R.id.activity_daftar_mahasiswa_bimbingan), "Gagal menerima respon dari server", Snackbar.LENGTH_INDEFINITE)
                            .setAction("Ulangi", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    loadDataMahasiswa();
                                }
                            })
                            .show();
                }
            }

            @Override
            public void onFailure(Call<List<MahasiswaDb>> call, Throwable t) {
                pd.dismiss();
                t.printStackTrace();
                Snackbar.make(findViewById(R.id.activity_daftar_mahasiswa_bimbingan), "Gagal terhubung ke server", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Ulangi", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loadDataMahasiswa();
                            }
                        })
                        .show();
            }
        });
    }
}
