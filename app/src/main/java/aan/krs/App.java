package aan.krs;

import android.content.Intent;

import com.orm.SugarApp;

import aan.krs.services.CheckSetujuiKRSService;
import aan.krs.services.CheckUpdateKRSService;

/**
 * Created by azisa on 2/10/2017.
 */

public class App extends SugarApp {

    @Override
    public void onCreate() {

        super.onCreate();
        if(Pref.getInstance(this).isLoginIn() && Pref.getInstance(this).getDataMahasiswa() != null){
            CheckSetujuiKRSService.startAction(this,2016,2);
            startService(new Intent(this, CheckUpdateKRSService.class));
        }

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

}
