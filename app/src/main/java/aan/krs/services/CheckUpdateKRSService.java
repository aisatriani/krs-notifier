package aan.krs.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.Calendar;
import java.util.List;

import aan.krs.Pref;
import aan.krs.api.ApiGenerator;
import aan.krs.api.ApiResponse;
import aan.krs.api.HttpClientService;
import aan.krs.db.DataKRS;
import aan.krs.model.Krs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckUpdateKRSService extends Service {
    private static final int INTERVAL = 15;
    private static final String TAG = CheckUpdateKRSService.class.getCanonicalName();

    public CheckUpdateKRSService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand: check update krs service");
        getAllKrs();

        return START_NOT_STICKY;
    }

    public void getAllKrs()
    {

        HttpClientService clientService = ApiGenerator.createService(HttpClientService.class);
        Call<ApiResponse<List<Krs.MataKuliah>>> call = clientService.cariKRS(2016, 2, Pref.getInstance(this).getNim());
        call.enqueue(new Callback<ApiResponse<List<Krs.MataKuliah>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<Krs.MataKuliah>>> call, Response<ApiResponse<List<Krs.MataKuliah>>> response) {
                if(response.isSuccessful()){

                    List<DataKRS> dataKRSes = DataKRS.listAll(DataKRS.class);
                    if(response.body().getData().size() > dataKRSes.size()){
                        Log.e(TAG, "onResponse: ada mata kuliah baru di kontrak");
                        DataKRS.deleteAll(DataKRS.class);
                        for(Krs.MataKuliah mk : response.body().getData()){
                            DataKRS data = new DataKRS();
                            data.setKeterangan(mk.getKETERANGAN());
                            data.setStatus(mk.getSTATUS());
                            data.setKodemakul(mk.getKODEMAKUL());
                            data.setNamamakul(mk.getNAMAMAKUL());
                            data.save();
                        }
                    }

                    stopSelf();
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<Krs.MataKuliah>>> call, Throwable t) {
                getAllKrs();
            }
        });
    }

    private void retryService() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, INTERVAL);

        AlarmManager alarm = (AlarmManager)getSystemService(ALARM_SERVICE);
        alarm.set(
                AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(),
                PendingIntent.getService(this, 0, new Intent(this, CheckUpdateKRSService.class), 0)
        );
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy: sevice destroy" );
        if(Pref.getInstance(this).isLoginIn())
            retryService();

    }
}
