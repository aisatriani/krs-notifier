package aan.krs.services;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import aan.krs.KrsActivity;
import aan.krs.MainActivity;
import aan.krs.Pref;
import aan.krs.R;
import aan.krs.api.ApiGenerator;
import aan.krs.api.ApiResponse;
import aan.krs.api.HttpClientService;
import aan.krs.db.DataKRS;
import aan.krs.model.Krs;
import retrofit2.Call;
import retrofit2.Response;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class CheckSetujuiKRSService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_CHECK = "aan.krs.action.FOO";

    // TODO: Rename parameters
    private static final String EXTRA_TAHUN = "aan.krs.extra.PARAM1";
    private static final String EXTRA_SEMESTER = "aan.krs.extra.PARAM2";
    private static final int INTERVAL = 30;
    private static final String TAG = "CHECKUPDATE";
    private Intent intentx;

    public CheckSetujuiKRSService() {
        super("CheckSetujuiKRSService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startAction(Context context, int tahun, int semester) {
        Intent intent = new Intent(context, CheckSetujuiKRSService.class);
        intent.setAction(ACTION_CHECK);
        intent.putExtra(EXTRA_TAHUN, tahun);
        intent.putExtra(EXTRA_SEMESTER, semester);
        context.startService(intent);
        System.out.println("service started...!!!");
    }



    @Override
    protected void onHandleIntent(Intent intent) {
        intentx = intent;
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_CHECK.equals(action)) {
                final int param1 = intent.getIntExtra(EXTRA_TAHUN,0);
                final int param2 = intent.getIntExtra(EXTRA_SEMESTER,0);
                handleActionCheck(param1, param2);
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionCheck(int tahun, int semester) {

        List<DataKRS> dataKRSes = DataKRS.listAll(DataKRS.class);
        for(DataKRS data : dataKRSes){

            HttpClientService clientService = ApiGenerator.createService(HttpClientService.class);
            Call<ApiResponse<List<Krs.MataKuliah>>> call = clientService.cariKRS(tahun, semester, Pref.getInstance(this).getNim());

            try {

                Response<ApiResponse<List<Krs.MataKuliah>>> execute = call.execute();
                ApiResponse<List<Krs.MataKuliah>> body = execute.body();
                List<Krs.MataKuliah> mataKuliahs = body.getData();
                for (Krs.MataKuliah mk : mataKuliahs){
                    System.out.println("cek mata kuliah"+ mk.getNAMAMAKUL() );
                    if(data.getKodemakul().equals(mk.getKODEMAKUL())){
                        if(data.getStatus() == 0 && mk.getSTATUS() == 1){
                            sendNotification(data.getNamamakul() + " telah disetujui");
                            data.setStatus(1);
                            data.save();
                        }
                        else if(data.getStatus() == 0 && mk.getSTATUS() == 2){
                            sendNotification(data.getNamamakul() + " tidak disetujui");
                            data.setStatus(2);
                            data.save();
                        }
                        else if(data.getStatus() == 0 && mk.getSTATUS() == 3){
                            sendNotification(data.getNamamakul() + " di pending (summer)");
                            data.setStatus(3);
                            data.save();
                        }
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        stopSelf();

    }

    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, KrsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("tahun","2016");
        intent.putExtra("semester", "2");

        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int)System.currentTimeMillis() /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.krs)
                .setContentTitle("KRS Notifier")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify((int)System.currentTimeMillis() /* ID of notification */, notificationBuilder.build());

    }

    private void retryService() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, INTERVAL);

        AlarmManager alarm = (AlarmManager)getSystemService(ALARM_SERVICE);
        alarm.set(
                AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(),
                PendingIntent.getService(this, 0, intentx, 0)
        );
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy: sevice destroy" );
        if(Pref.getInstance(this).isLoginIn())
            retryService();

    }
}
