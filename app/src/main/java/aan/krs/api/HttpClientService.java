package aan.krs.api;

import java.util.List;

import aan.krs.model.Dosen;
import aan.krs.model.KontrakKrs;
import aan.krs.model.Krs;
import aan.krs.model.Mahasiswa;
import aan.krs.model.MahasiswaDb;
import aan.krs.model.StatusKRS;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by azisa on 1/18/2017.
 */

public interface HttpClientService {

    @FormUrlEncoded
    @POST("loginmahasiswa")
    Call<ApiResponse<Mahasiswa>> loginMahasiswa(@Field("nim") String nim, @Field("pwd") String pwd);

    @FormUrlEncoded
    @POST("logindosen")
    Call<ApiResponse<Dosen>> loginDosen(@Field("nidn") String nidn, @Field("pwd") String pwd);

    @GET("demo/krs")
    Call<List<KontrakKrs>> getKontrakKrs(
            @Query("tahun") int tahun,
            @Query("semester") int semester,
            @Query("nim") String nim);

    @GET("demo/bimbingan/{nidn}")
    Call<List<MahasiswaDb>> getMahasiswaBimbingan(@Path("nidn") String nidn);

    @GET("demo/kontrak/{nidn}")
    Call<List<KontrakKrs>> getMahasiswaKontrakKrs(@Path("nidn") String nidn);

    @GET("demo/kontrak/{nim}/{tahun}/{semester}")
    Call<List<KontrakKrs>> getMatakulihKontrak(@Path("nim") String nim, @Path("tahun") int tahun, @Path("semester") int semester);

    @GET("krs/{tahun}/{semester}/{nim}/all")
    public Call<ApiResponse<List<Krs>>> getKrs(@Path("tahun") int tahun, @Path("semester") int semester, @Path("nim") String nim);

    @GET("statuskrs/{tahun}/{semester}/{nim}/{idmakul}")
    public Call<StatusKRS> getStatusKrs(@Path("tahun") int tahun,
                                        @Path("semester") int semester,
                                        @Path("nim") String nim,
                                        @Path("idmakul") String idmakul);

    @GET("carikrs/{tahun}/{semester}/{nim}")
    public Call<ApiResponse<List<Krs.MataKuliah>>> cariKRS(@Path("tahun") int tahun, @Path("semester") int semester, @Path("nim") String nim);

}
