package aan.krs;

import android.app.ProgressDialog;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import java.util.List;

import aan.krs.adapter.KontrakKrsAdapter;
import aan.krs.api.ApiGenerator;
import aan.krs.api.HttpClientService;
import aan.krs.model.KontrakKrs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KontrakKrsActivity extends AppCompatActivity {

    private static final int INTERVAL = 30;
    private RecyclerView rv;
    private String tahun, strSemester;
    private LinearLayout notFoundLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kontrak_krs);

        tahun = getIntent().getStringExtra("tahun");
        strSemester = getIntent().getStringExtra("semester");

        notFoundLayout = (LinearLayout)findViewById(R.id.not_found_box);
        notFoundLayout.setVisibility(View.GONE);

        rv = (RecyclerView)findViewById(R.id.recyclerView);
        rv.setLayoutManager(new LinearLayoutManager(this));

        Log.d("KONTRAKKRS",tahun +" - "+ strSemester);

        loadKontrakKrs();

    }

    private void loadKontrakKrs() {

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Loading..");
        pd.setCanceledOnTouchOutside(false);
        pd.show();



        HttpClientService clientService = ApiGenerator.createService(HttpClientService.class);
        Call<List<KontrakKrs>> call = clientService.getKontrakKrs(Integer.valueOf(tahun), getIntSemester(strSemester), Pref.getInstance(this).getNim());
        call.enqueue(new Callback<List<KontrakKrs>>() {
            @Override
            public void onResponse(Call<List<KontrakKrs>> call, Response<List<KontrakKrs>> response) {
                pd.dismiss();
                if(response.isSuccessful()){
                    KontrakKrsAdapter adapter = new KontrakKrsAdapter(KontrakKrsActivity.this, response.body());
                    rv.setAdapter(adapter);
                }else{
                    Snackbar.make(findViewById(R.id.activity_kontrak_krs), "Terjadi Kesalahan..", Snackbar.LENGTH_INDEFINITE)
                            .setAction("Ulangi", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    loadKontrakKrs();
                                }
                            })
                            .show();
                }
            }

            @Override
            public void onFailure(Call<List<KontrakKrs>> call, Throwable t) {
                pd.dismiss();
                t.printStackTrace();

                Snackbar.make(findViewById(R.id.activity_kontrak_krs), "Gagal terhubung ke server", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Ulangi", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loadKontrakKrs();
                            }
                        })
                        .show();
            }
        });

    }

    private int getIntSemester(String strSemester) {
        String str = strSemester.toLowerCase();
        int nilai = 0;

        if(str.equals("ganjil"))
            nilai = 1;
        if(str.equals("genap"))
            nilai = 2;

        if(str.equals("1")){
            nilai = 1;
        }
        if(str.equals("2")){
            nilai = 2;
        }


        return nilai;
    }


}
