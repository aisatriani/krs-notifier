package aan.krs.model;

import java.io.Serializable;

/**
 * Created by azisa on 1/29/2017.
 */

public class KontrakKrs implements Serializable {

    private int id;
    private String nim;
    private String nama;
    private String kodemakul;
    private String namamakul;
    private String pa;
    private int setujui;
    private int tahun;
    private int semester;
    private String created_at;
    private String updated_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKodemakul() {
        return kodemakul;
    }

    public void setKodemakul(String kodemakul) {
        this.kodemakul = kodemakul;
    }

    public String getNamamakul() {
        return namamakul;
    }

    public void setNamamakul(String namamakul) {
        this.namamakul = namamakul;
    }

    public String getPa() {
        return pa;
    }

    public void setPa(String pa) {
        this.pa = pa;
    }

    public int getSetujui() {
        return setujui;
    }

    public void setSetujui(int setujui) {
        this.setujui = setujui;
    }

    public int getTahun() {
        return tahun;
    }

    public void setTahun(int tahun) {
        this.tahun = tahun;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
