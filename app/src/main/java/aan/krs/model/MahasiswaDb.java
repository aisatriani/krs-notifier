package aan.krs.model;

import java.io.Serializable;

/**
 * Created by azisa on 2/3/2017.
 */

public class MahasiswaDb implements Serializable{
    private int id;
    private String nim;
    private String nama;
    private String kelas;
    private String dosen_pa_nidn;
    private String dosen_pa_nama;
    private String kodeprodi;
    private String angkatan;
    private String status;
    private String created_at;
    private String updated_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getDosen_pa_nidn() {
        return dosen_pa_nidn;
    }

    public void setDosen_pa_nidn(String dosen_pa_nidn) {
        this.dosen_pa_nidn = dosen_pa_nidn;
    }

    public String getDosen_pa_nama() {
        return dosen_pa_nama;
    }

    public void setDosen_pa_nama(String dosen_pa_nama) {
        this.dosen_pa_nama = dosen_pa_nama;
    }

    public String getKodeprodi() {
        return kodeprodi;
    }

    public void setKodeprodi(String kodeprodi) {
        this.kodeprodi = kodeprodi;
    }

    public String getAngkatan() {
        return angkatan;
    }

    public void setAngkatan(String angkatan) {
        this.angkatan = angkatan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
