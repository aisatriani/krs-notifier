package aan.krs.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by azisa on 1/14/2017.
 */

public class Krs implements Serializable {

    private String KODEFAK;
    private String FAKULTAS;
    private String KODEPRODI;
    private String PRODI;
    private String JENJANG;
    private Map<String, List<MataKuliah>> MAHASISWA;

    public class MataKuliah implements Serializable {
        String KODEMAKUL;
        String NAMAMAKUL;
        String KELAS;
        String SKS;
        String BOBOT;
        String SIMBOL;
        int STATUS;
        String KETERANGAN;
        String TANGGALKRS;
        String TANGGALAPPROVAL;

        public String getKODEMAKUL() {
            return KODEMAKUL;
        }

        public void setKODEMAKUL(String KODEMAKUL) {
            this.KODEMAKUL = KODEMAKUL;
        }

        public String getNAMAMAKUL() {
            return NAMAMAKUL;
        }

        public void setNAMAMAKUL(String NAMAMAKUL) {
            this.NAMAMAKUL = NAMAMAKUL;
        }

        public String getKELAS() {
            return KELAS;
        }

        public void setKELAS(String KELAS) {
            this.KELAS = KELAS;
        }

        public String getSKS() {
            return SKS;
        }

        public void setSKS(String SKS) {
            this.SKS = SKS;
        }

        public String getBOBOT() {
            return BOBOT;
        }

        public void setBOBOT(String BOBOT) {
            this.BOBOT = BOBOT;
        }

        public String getSIMBOL() {
            return SIMBOL;
        }

        public void setSIMBOL(String SIMBOL) {
            this.SIMBOL = SIMBOL;
        }

        public int getSTATUS() {
            return STATUS;
        }

        public void setSTATUS(int STATUS) {
            this.STATUS = STATUS;
        }

        public String getKETERANGAN() {
            return KETERANGAN;
        }

        public void setKETERANGAN(String KETERANGAN) {
            this.KETERANGAN = KETERANGAN;
        }

        public String getTANGGALKRS() {
            return TANGGALKRS;
        }

        public void setTANGGALKRS(String TANGGALKRS) {
            this.TANGGALKRS = TANGGALKRS;
        }

        public String getTANGGALAPPROVAL() {
            return TANGGALAPPROVAL;
        }

        public void setTANGGALAPPROVAL(String TANGGALAPPROVAL) {
            this.TANGGALAPPROVAL = TANGGALAPPROVAL;
        }
    }

    public String getKODEFAK() {
        return KODEFAK;
    }

    public void setKODEFAK(String KODEFAK) {
        this.KODEFAK = KODEFAK;
    }

    public String getFAKULTAS() {
        return FAKULTAS;
    }

    public void setFAKULTAS(String FAKULTAS) {
        this.FAKULTAS = FAKULTAS;
    }

    public String getKODEPRODI() {
        return KODEPRODI;
    }

    public void setKODEPRODI(String KODEPRODI) {
        this.KODEPRODI = KODEPRODI;
    }

    public String getPRODI() {
        return PRODI;
    }

    public void setPRODI(String PRODI) {
        this.PRODI = PRODI;
    }

    public String getJENJANG() {
        return JENJANG;
    }

    public void setJENJANG(String JENJANG) {
        this.JENJANG = JENJANG;
    }

    public Map<String, List<MataKuliah>> getMAHASISWA() {
        return MAHASISWA;
    }

    public void setMAHASISWA(Map<String, List<MataKuliah>> MAHASISWA) {
        this.MAHASISWA = MAHASISWA;
    }
}
