package aan.krs.model;

/**
 * Created by azisa on 2/10/2017.
 */

public class StatusKRS {

    private int err_no;
    private String err_teks;
    private String STATUS;
    private String KETERANGAN;

    public int getErr_no() {
        return err_no;
    }

    public void setErr_no(int err_no) {
        this.err_no = err_no;
    }

    public String getErr_teks() {
        return err_teks;
    }

    public void setErr_teks(String err_teks) {
        this.err_teks = err_teks;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getKETERANGAN() {
        return KETERANGAN;
    }

    public void setKETERANGAN(String KETERANGAN) {
        this.KETERANGAN = KETERANGAN;
    }
}
