/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aan.krs.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import aan.krs.DetailsKontrakActivity;
import aan.krs.KontrakKrsActivity;
import aan.krs.MainActivity;
import aan.krs.R;
import aan.krs.model.KontrakKrs;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        KontrakKrs kontrakKrs = new KontrakKrs();

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            if(remoteMessage.getData().get("type").equals("kontrak_krs") || remoteMessage.getData().get("type").equals("approve_krs")){
                kontrakKrs.setCreated_at(remoteMessage.getData().get("created_at"));
                kontrakKrs.setId(Integer.valueOf(remoteMessage.getData().get("id")));
                kontrakKrs.setKodemakul(remoteMessage.getData().get("kodemakul"));
                kontrakKrs.setNamamakul(remoteMessage.getData().get("namamakul"));
                kontrakKrs.setNim(remoteMessage.getData().get("nim"));
                kontrakKrs.setPa(remoteMessage.getData().get("pa"));
                kontrakKrs.setTahun(Integer.valueOf(remoteMessage.getData().get("tahun")));
                kontrakKrs.setSemester(Integer.valueOf(remoteMessage.getData().get("semester")));
                kontrakKrs.setUpdated_at(remoteMessage.getData().get("updated_at"));
            }


        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            if(remoteMessage.getData().get("type").equals("kontrak_krs")){
                sendNotificationKontrakKrs(remoteMessage.getNotification().getBody(), kontrakKrs);
            }else if(remoteMessage.getData().get("type").equals("approve_krs")){
                sendNotificationApprove(remoteMessage.getNotification().getBody(), remoteMessage.getData().get("semester"), kontrakKrs);
            }else{
                sendNotification(remoteMessage.getNotification().getBody());
            }

        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private void sendNotificationApprove(String body, String semester, KontrakKrs kontrakKrs) {
        Intent intent = new Intent(this, KontrakKrsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("mk_kontrak",kontrakKrs);
        intent.putExtra("tahun", String.valueOf(kontrakKrs.getTahun()));
        intent.putExtra("semester",semester);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int)System.currentTimeMillis() /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.krs)
                .setContentTitle("KRS Notifier")
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify((int)System.currentTimeMillis() /* ID of notification */, notificationBuilder.build());
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int)System.currentTimeMillis() /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.krs)
                .setContentTitle("KRS Notifier")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify((int)System.currentTimeMillis() /* ID of notification */, notificationBuilder.build());
    }

    private void sendNotificationKontrakKrs(String messageBody, KontrakKrs kontrakKrs) {
        Intent intent = new Intent(this, DetailsKontrakActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("mk_kontrak",kontrakKrs);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int)System.currentTimeMillis() /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.krs)
                .setContentTitle("KRS Notifier")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify((int)System.currentTimeMillis() /* ID of notification */, notificationBuilder.build());
    }
}
