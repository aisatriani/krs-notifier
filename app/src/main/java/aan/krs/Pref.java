package aan.krs;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;

import aan.krs.model.Dosen;
import aan.krs.model.Mahasiswa;


/**
 * Created by azisa on 12/12/2016.
 */
public class Pref {
    private static final String PREF_NAME = "krs_notifier";
    private static final String KEY_LATITUDE = "key_latitude";
    private static final String KEY_LONGITUDE = "key_longitude";
    private static final String KEY_DATA_MAHASISWA = "key_data_mahasiswa";
    private static final String KEY_DATA_DOSEN = "key_dosen";
    private static final String KEY_LOGIN = "key_login";
    private static final String KEY_NIM = "key_nim";
    private static final String KEY_LOKASI_STATE = "key_lokasi_state";
    private static final String KEY_LOGIN_STATE = "key_login_state";
    private static final String KEY_NIDN = "key_nidn";
    private static final String KEY_COUNT_KRS = "key_count_krs";
    private static Pref ourInstance;
    private final Context context;
    private final SharedPreferences pref;

    public static Pref getInstance(Context ctx) {
        if(ourInstance == null){
            ourInstance = new Pref(ctx);
        }
        return ourInstance;
    }

    private Pref(Context ctx) {
        this.context = ctx;
        this.pref = ctx.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public void storeMyPosition(double latitude, double longitude){
        SharedPreferences.Editor editor = pref.edit();
        editor.putLong(KEY_LATITUDE,Double.doubleToLongBits(latitude));
        editor.putLong(KEY_LONGITUDE, Double.doubleToLongBits(longitude));
        editor.apply();
    }



    public void storeDataMahasiswa(Mahasiswa mahasiswa){

        Gson gson = new Gson();
        String json = gson.toJson(mahasiswa);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(KEY_DATA_MAHASISWA, json);
        editor.apply();
    }

    public void storeDataDosen(Dosen dosen){

        Gson gson = new Gson();
        String json = gson.toJson(dosen);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(KEY_DATA_DOSEN, json);
        editor.apply();
    }

    public Mahasiswa getDataMahasiswa(){
        Gson gson = new Gson();
        String json = pref.getString(KEY_DATA_MAHASISWA, null);
        Mahasiswa mahasiswa = gson.fromJson(json, Mahasiswa.class);
        return mahasiswa;
    }

    public Dosen getDataDosen(){
        Gson gson = new Gson();
        String json = pref.getString(KEY_DATA_DOSEN, null);
        Dosen dosen = gson.fromJson(json, Dosen.class);
        return dosen;
    }

    public void setLoginIn(boolean loginIn){
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(KEY_LOGIN, loginIn);
        editor.apply();
    }

    public boolean isLoginIn(){
        boolean login = pref.getBoolean(KEY_LOGIN, false);
        return login;
    }

    public void storeNim(String nim){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(KEY_NIM, nim);
        editor.apply();
    }

    public String getNim(){
        String nim = pref.getString(KEY_NIM, null);
        return nim;
    }

    public void storeNidn(String nidn){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(KEY_NIDN, nidn);
        editor.apply();
    }

    public String getNidn(){
        String nidn = pref.getString(KEY_NIDN, null);
        return nidn;
    }

    public void clearAllData(){
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.apply();
    }

    public void storeLoginState(int state)
    {
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(KEY_LOGIN_STATE, state);
        editor.apply();
    }

    public int getLoginState()
    {
        int state = pref.getInt(KEY_LOGIN_STATE, 0);
        return state;
    }

    public void setUpdateLokasiDosen(boolean state){
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(KEY_LOKASI_STATE, state);
        editor.apply();
    }

    public boolean isEnableUpdateLocation()
    {
        boolean state = pref.getBoolean(KEY_LOKASI_STATE, true);
        return state;
    }

    public void setCountKRS(int countKRS){
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(KEY_COUNT_KRS, countKRS);
        editor.apply();
    }

    public int getCountKRS(){
        int count = pref.getInt(KEY_COUNT_KRS,0);
        return count;
    }


    public boolean isFirstLoad() {
        boolean firtsload = pref.getBoolean("firstload",false);
        return firtsload;
    }

    public void setFirstLoad(boolean first){
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("firstload",first);
        editor.apply();
    }
}
