package aan.krs.db;

import com.orm.SugarRecord;

/**
 * Created by azisa on 2/10/2017.
 */

public class DataKRS extends SugarRecord {

    private String kodemakul;
    private String namamakul;
    private int status;
    private String keterangan;

    public String getKodemakul() {
        return kodemakul;
    }

    public void setKodemakul(String kodemakul) {
        this.kodemakul = kodemakul;
    }

    public String getNamamakul() {
        return namamakul;
    }

    public void setNamamakul(String namamakul) {
        this.namamakul = namamakul;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
